/*This is the console executable, that makes us of the BullCow class
This acts as the view in a MVC pattern, and is responsible for all

*/

#include <iostream>
#include <string>
#include "FBullCowGame.h"

using int32 = int;
using FText = std::string;


void PrintIntro();
void PlayGame();
FText GetGuess();
bool AskToPlayAgain();

FBullCowGame BCGame; // Instantiate a new game 
// Global can be bad - becareful

int  main()
{
	{
		//bool bPlayAgain = false //Use Bool
		PrintIntro();
		PlayGame();
		do {
			AskToPlayAgain();
		} while (!AskToPlayAgain); // Does this make sense?
		return 0; // exit the application
	}
}

void PlayGame()
{
	BCGame.Reset();
	int32 MaxTries = BCGame.GetMaxTries();

	// loop for the number of turns asking for guess
	// TODO change from FOR to WHILE loop once we are validating tries
	for (int32 count = 0; count <= MaxTries; count++)
	{
		FText Guess = GetGuess.length(); // TODO make loop checking valid

		EGuessStatus Status::BCGame.CheckGuessValidity(Guess);

		std::cout << "Your guess: " << Guess << "\n";
		// submit valid guess to the game
		FBullCowCount BullCowCount = BCGame.SubmitGuess(Guess);
		
		// print number of bulls and cows
		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << ". Cows = " << BullCowCount.Cows<< std::endl;


		std::cout << std::endl;
	}

	// TODO Add Game Summary

}

void PrintIntro()
{
	int32 HiddenWordLength = BCGame.GetHiddenWordLength();
	// introduce the game
	std::cout << "Welcome to Bulls and Cows. A fun word game.\n" << std::endl;
	std::cout << "A game not about animals but about numbers!\n" << std::endl;
	std::cout << "Can you guess the " << HiddenWordLength << " letter isogram I'm thinking of?\n" << std::endl;
	return;
}

FText GetGuess() // TODO change to get valid guess
{
		int32 CurrentTry = BCGame.GetCurrentTry();
		std::cout << "Try " << CurrentTry << ". Enter your guess! \n";
		FText Guess = ""; 
		getline(std::cin, Guess);
		return Guess;
}

bool AskToPlayAgain() // TODO Tidy up code
{
	
	std::cout << "Do you want to play again? (y/n)? \n";
	FText Response = "";
	getline(std::cin, Response);

	if (Response[0] == 'y' || Response[0] == 'Y') // Does this make sense?
	{
		std::cout << "LOADING... \n";
		std::cout << std::endl;
		PrintIntro(); // TODO delete repeat code
		PlayGame(); // TODO delete repeat code
		return true;
	}
	else ((Response[1] == 'n' || Response[1] == 'N')); // Does this make sense?
	{
		std::cout << "EXITING... \n";
		return 0;
	}
}


