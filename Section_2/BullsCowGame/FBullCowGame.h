#pragma once
#include <string>

using FString = std::string;
using int32 = int;

struct FBullCowCount 
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum EGuessStatus

{
	Ok,
	Not_Isogram,
	Not_lowercase,
	Not_letters,
	Wrong_Length

};

class FBullCowGame {
public:
	FBullCowGame(); // constructor

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength()const;
	bool IsGameWon() const; // Did you win or lose?
	EGuessStatus CheckGuessValidity(FString) const; 

	void Reset(); // TODO make a more rich return value.

	// provide a method for counting bulls and cows, and increasing try #
	FBullCowCount SubmitGuess(FString);

// ^^ Please try and ignore this and focus on interface above ^^
private:
	// see constructor for initilisation
	int32 MyCurrentTry;
	int32 MyMaxTries;
	FString MyHiddenWord;
	int32 MyHiddenWordLength;
	


};